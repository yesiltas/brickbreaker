﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedBrick : BrickBase
{
    Renderer rend;

    private void Awake()
    {
        rend = GetComponent<Renderer>();
    }

    public override void OnInitialize()
    {
        health = 75;
        rend.material.SetColor("_Color", Color.red);
    }
}
