﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodBrick : BrickBase
{
    Renderer rend;

    private void Awake()
    {
        rend = GetComponent<Renderer>();
    }

    public override void OnInitialize()
    {
        health = 25;
        Color color = new Color(0.6484375f, 0.5f, 0.390625f, 1);
        rend.material.SetColor("_Color", color);
    }
}
