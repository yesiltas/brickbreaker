﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IronBrick : BrickBase
{
    Renderer rend;

    private void Awake()
    {
        rend = GetComponent<Renderer>();
    }

    public override void OnInitialize()
    {
        health = 100;
        Color color = new Color(0.1608f, 0.2157f, 0.2863f, 1);
        rend.material.SetColor("_Color", color);
    }
}
