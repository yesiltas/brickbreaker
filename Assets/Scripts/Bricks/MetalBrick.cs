﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MetalBrick : BrickBase
{
    Renderer rend;

    private void Awake()
    {
        rend = GetComponent<Renderer>();
    }

    public override void OnInitialize()
    {
        health = 50;
        Color color = new Color(0.651f, 0.7059f, 0.6706f, 1);
        rend.material.SetColor("_Color", color);
    }
}
