﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickBase : MonoBehaviour
{
    public enum BrickType
    {
        wood,
        metal,
        iron,
        red,
        green
    }

    public BrickType brickType;

    public int health;

    public bool isPowerup;

    public bool isBreakable;

    public PowerUpBase powerUpBase;

    public virtual void Initialize(bool isPowerup)
    {
        this.isPowerup = isPowerup;

        gameObject.SetActive(true);
        OnInitialize();
    }

    public void UpdatePowerUp()
    {
        if (isPowerup)
        {
            PowerUpManager.instance.Create(gameObject.GetComponent<BrickBase>());
        }
    }

    public virtual void OnInitialize()
    {

    }

    public virtual void Hit(bool ispowerup)
    {
        if (isBreakable)
        {
            health -= 25;
            if (health <= 0)
            {
                if (isPowerup)
                {
                    GameManager.instance.PowerUpMakeActive(powerUpBase);
                }
                gameObject.SetActive(false);
            }
        }
    }
}




