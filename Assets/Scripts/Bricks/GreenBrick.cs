﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenBrick : BrickBase
{
    Renderer rend;

    private void Awake()
    {
        rend = GetComponent<Renderer>();
    }

    public override void OnInitialize()
    {
        health = 50;
        rend.material.SetColor("_Color", Color.green);
    }
}
