﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour
{
    RaycastHit hit;

    Vector3 mouseposition;

    Ray ray;

    Vector3 startposion;

    Vector3 playerscale;

    bool isGamePlaying;

    private void Awake()
    {
        startposion = transform.position;
        playerscale = transform.localScale;
    }

    void Start()
    {
        GameManager.GameStartEvent += OnGameStarted;
    }

    void OnGameStarted(int brickcount)
    {
        transform.position = startposion;
        transform.localScale = playerscale;
        isGamePlaying = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Target")
        {
            GameManager.instance.PlayerCaughtPowerUp(other.GetComponent<PowerUpBase>());
        }
    }

    public float PlayerMaxMovement(float x_value)
    {
        float x_point = 9.75f-(gameObject.GetComponent<BoxCollider>().bounds.size.x * 0.5f);
        float playerMaxMovement = Mathf.Clamp(x_value, -x_point, x_point);
        return playerMaxMovement;
    }

    void Update()
    {
        if (isGamePlaying == false)
            return;

        if (Input.GetMouseButton(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.tag == "Ground")
                {
                    mouseposition = new Vector3(PlayerMaxMovement(hit.point.x), 0.5f, -9.0f);

                    transform.position = mouseposition;
                }
            }
        }
    }
}
