﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public UIManager uiManager;

    public BrickManager bricksManager;

    public static Action<int> GameStartEvent;

    public static Action<bool> GameFinishEvent;

    public static GameManager instance;

    public int score = 0;

    int totalBrickCount = 104;

    private void Awake()
    {
        instance = this;
    }

    public void StartGame(int brickcount)
    {
        GameStartEvent(brickcount);
    }

    void FinishGame(bool result)
    {
        score = 0;
        GameFinishEvent(result);
    }

    public void PlayerHitBrick(GameObject brick)
    {
        score++;
        uiManager.UpdatePlayerScore(score);
        if (score >= totalBrickCount)
        {
            FinishGame(true);
        }
        BrickBase hitBrick = brick.GetComponent<BrickBase>();
        if (hitBrick)
        {
            hitBrick.Hit(hitBrick.isPowerup);
        }
    }

    public void PowerUpMakeActive(PowerUpBase powerUpBase)
    {
        powerUpBase.Hit();
    }

    public void PlayerCaughtPowerUp(PowerUpBase powerUpBase)
    {
        powerUpBase.isColliding = true;
    }

    public void PlayerDie(BallContoller ballcontroller)
    {
        BallManager.instance.Destroyball(ballcontroller);
        if (BallManager.instance.ballContollerList.Count == 0)
            FinishGame(false);
    }
}
