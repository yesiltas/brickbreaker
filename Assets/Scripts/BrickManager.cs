﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickManager : MonoBehaviour
{

    public List<BrickBase> brickList = new List<BrickBase>();

    public List<BrickBase> brickPrefabs = new List<BrickBase>();

    void Start()
    {
        GameManager.GameStartEvent += OnGameStarted;
        GameManager.GameFinishEvent += OnGameFinished;
    }

    private void OnGameFinished(bool obj)
    {
        DestroyBricks();
    }

    private void DestroyBricks()
    {
        foreach (var item in brickList)
        {
            Destroy(item.gameObject);
        }
        brickList.Clear();
    }

    void OnGameStarted(int brickcount)
    {
        CreateBricks(brickcount);
        SetBrickPosition(brickcount);
        UpdateBricksPowerUps();
    }

    void UpdateBricksPowerUps()
    {
        foreach (var item in brickList)
        {
            item.UpdatePowerUp();
        }
    }

    void CreateBricks(int brickcount)
    {
        float powerUpChance = 0.2f;
        int maxPowerUpCount = (int)(brickcount * powerUpChance);

        List<int> powerUpIndex = new List<int>();

        for (int i = 0; i < maxPowerUpCount; i++)
        {
            int randomIndex = GetPowerUpIndex(0, brickcount);
            if (powerUpIndex.Contains(randomIndex))
            {
                i--;
            }
            else powerUpIndex.Add(randomIndex);
        }

        for (int i = 0; i < brickcount; i++)
        {
            bool isPowerUp = powerUpIndex.Contains(i);
            Create(isPowerUp, GetRandomrefab());
        }
    }

    GameObject GetRandomrefab()
    {
        int randomIndex = UnityEngine.Random.Range(0, brickPrefabs.Count);
        randomIndex = 2;
        return brickPrefabs[randomIndex].gameObject;

    }

    int GetPowerUpIndex(int powerupindex, int brickcount)
    {
        powerupindex = UnityEngine.Random.Range(powerupindex, brickcount);
        return powerupindex;
    }

    void Create(bool ispowerup, GameObject prefabrick)
    {
        BrickBase brick = Instantiate(prefabrick).GetComponent<BrickBase>();
        brick.Initialize(ispowerup);

        brickList.Add(brick);
    }

    void SetBrickPosition(int brickcount)
    {
        float x = -6.3f;
        float increase = 1.1f;
        float z = 7;
        brickcount--;

        for (int i = 0; i < 8; i++)
        {
            if (brickcount < 12)
                increase = 12.0f / (float)brickcount;

            for (int j = 0; j < 13; j++)
            {
                if (brickcount < 0)
                    break;

                brickList[brickcount].gameObject.transform.position = new Vector3(x, 0.5f, z);
                brickcount--;
                x += increase;
            }
            z -= 1.1f;
            x = -6.3f;
        }

    }
}


