﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpManager : MonoBehaviour
{
    public List<PowerUpBase> powerUpList = new List<PowerUpBase>();

    public GameObject prefabpowerupFast;

    public GameObject prefabpowerupSlow;

    public GameObject prefabpowerupBigger;

    public GameObject prefabpowerupSmaller;

    public GameObject prefabpowerupDouble;

    public static PowerUpManager instance;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        GameManager.GameFinishEvent += OnGameFinished;
    }

    private void OnGameFinished(bool obj)
    {
        DestroyAllPowerUp();
    }

    private void DestroyAllPowerUp()
    {
        foreach (var item in powerUpList)
        {
            Destroy(item.gameObject);
        }
        powerUpList.Clear();
    }

    public void DestroyPowerUp(PowerUpBase powerUpBase)
    {
        Destroy(powerUpBase.gameObject);
        powerUpList.Remove(powerUpBase);
    }

    public void Create(BrickBase brick)
    {
        int type = UnityEngine.Random.Range(0, 5);

        switch (type)
        {
            case 0:
                InitializePowerUp(brick,prefabpowerupDouble);
                break;
            case 1:
                InitializePowerUp(brick, prefabpowerupFast);
                break;
            case 2:
                InitializePowerUp(brick,prefabpowerupSlow);
                break;
            case 3:
                InitializePowerUp(brick,prefabpowerupBigger);
                break;
            case 4:
                InitializePowerUp(brick,prefabpowerupSmaller);
                break;
            default:
                break;
        }
    }

    private void InitializePowerUp(BrickBase brick, GameObject prefab)
    {
        PowerUpBase powerUpBase = Instantiate(prefab).GetComponent<PowerUpBase>();
        powerUpBase.Initialize(brick.transform.position);
        powerUpList.Add(powerUpBase);
        brick.powerUpBase = powerUpBase;
    }
}
