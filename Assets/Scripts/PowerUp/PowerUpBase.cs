﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpBase : MonoBehaviour
{
    public bool movement;

    public enum PowerUpType
    {
        DoubleBall, Fast, Slow, Bigger, Smaller
    }

    public PowerUpType powerUpType;

    public int speed;

    public bool isColliding;

    public virtual void Initialize(Vector3 spawnPosition)
    {
        transform.position = spawnPosition;
        OnInitialize();
    }

    public virtual void OnInitialize()
    {

    }

    public void Hit()
    {
        gameObject.SetActive(true);
        movement = true;
    }

    public virtual void Movement()
    {
        transform.position += -Vector3.forward * speed * Time.deltaTime;
    }

    public void FixedUpdate()
    {
        if (movement)
        {
            if (gameObject.transform.position.z <= -14)
            {
                movement = false;
                gameObject.SetActive(false);
                return;
            }
            if (!isColliding)
            {
                Movement();
            }
            else
            {
                Power();
            }
        }
    }

    public virtual void Power()
    {

    }
}
