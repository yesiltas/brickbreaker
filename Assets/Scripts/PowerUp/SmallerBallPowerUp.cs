﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallerBallPowerUp : PowerUpBase
{
    Renderer rend;

    private void Awake()
    {
        rend = GetComponent<Renderer>();
    }

    public override void OnInitialize()
    {
        rend.material.SetColor("_Color", Color.magenta);
        gameObject.SetActive(false);
    }

    public override void Power()
    {
        BallManager.instance.Power(gameObject.GetComponent<SmallerBallPowerUp>());
    }
}
