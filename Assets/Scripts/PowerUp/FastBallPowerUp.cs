﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastBallPowerUp : PowerUpBase
{
    Renderer rend;

    private void Awake()
    {
        rend = GetComponent<Renderer>();
    }

    public override void OnInitialize()
    {
        rend.material.SetColor("_Color", Color.yellow);
        gameObject.SetActive(false);
    }

    public override void Power()
    {
        BallManager.instance.Power(gameObject.GetComponent<FastBallPowerUp>());
    }
}
