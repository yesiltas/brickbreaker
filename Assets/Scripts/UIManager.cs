﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject StartPanel;

    public GameObject FinishPanel;

    public GameObject GamePanel;

    public Text StartText;

    public Text Win_LoseText;

    public Text FinishText;

    public Text ScoreText;

    private void Awake()
    {
        Win_LoseText.text = "";

        StartText.text = "Tap To Start";

        FinishText.text = "Play Again";

        UpdatePlayerScore(0);
    }

    void Start()
    {       
        GameManager.GameStartEvent += OnGameStarted;
        GameManager.GameFinishEvent += OnGameFinished;
    }

    private void OnGameFinished(bool obj)
    {
        string result = obj == true ? "You Win" : "You Lose";
        OpenFinishPanel(result);
    }

    private void OpenFinishPanel(string result)
    {
        Win_LoseText.text = result;

        FinishPanel.SetActive(true);

        GamePanel.SetActive(false);

        StartPanel.SetActive(false);

        UpdatePlayerScore(0);
    }

    private void OnGameStarted(int brickcount)
    {
        OpenGamePanel();
    }


    private void OpenGamePanel()
    {
        GamePanel.SetActive(true);

        StartPanel.SetActive(false);

        FinishPanel.SetActive(false);
    }

    public void UpdatePlayerScore(int score)
    {
        ScoreText.text = "Score: " + score.ToString();
    }
}
