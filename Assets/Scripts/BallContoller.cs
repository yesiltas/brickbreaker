﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class BallContoller : MonoBehaviour
{
    Rigidbody rb;

    public float speed;

    public Vector3 direction;

    bool isGamePlaying;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void BallFirstMove()
    {
        isGamePlaying = true;
        this.direction = new Vector3(UnityEngine.Random.Range(9.5f, -9.5f), 0.5f, 9.5f);
    }

    void OnCollisionEnter(Collision collision)
    {
        ReflectDirection(collision.contacts[0].normal);

        if (collision.gameObject.tag == "Target")
        {
            HitBrick(collision.gameObject);
        }
    }

    void HitBrick(GameObject brick)
    {
        GameManager.instance.PlayerHitBrick(brick);
        
    }

    void ReflectDirection(Vector3 normal)
    {
        direction = Vector3.Reflect(direction, normal);
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Finish")
            Die();
    }

    void Die()
    {
        isGamePlaying = false;
        rb.velocity = Vector3.zero;
        GameManager.instance.PlayerDie(gameObject.GetComponent<BallContoller>());
    }

    void Movement()
    {
        rb.velocity = direction.normalized * speed;
    }

    void FixedUpdate()
    {
        if (isGamePlaying == false)
            return;
        Movement();
    }
}
