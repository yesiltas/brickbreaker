﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallManager : MonoBehaviour
{
    public GameObject prefabBall;

    public PlayerController player;

    public List<BallContoller> ballContollerList = new List<BallContoller>();

    public static BallManager instance;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        GameManager.GameStartEvent += OnGameStarted;
        GameManager.GameFinishEvent += OnGmaeFinished;
    }

    private void OnGmaeFinished(bool obj)
    {
        foreach (var item in ballContollerList)
        {
            Destroy(item.gameObject);
        }
        ballContollerList.Clear();
    }

    private void OnGameStarted(int obj)
    {
        CreateBall();
    }

    public void Power(PowerUpBase powerUpBase)
    {
        switch (powerUpBase.powerUpType.ToString())
        {
            case "DoubleBall":
                DoubleBall(powerUpBase);
                break;
            case "Fast":
                FastBall(powerUpBase);
                break;
            case "Slow":
                SlowBall(powerUpBase);
                break;
            case "Bigger":
                BiggerPlayer(powerUpBase);
                break;
            case "Smaller":
                SmallerPlayer(powerUpBase);
                break;
            default:
                break;
        }
    }

    private void SmallerPlayer(PowerUpBase powerUpBase)
    {
        PowerUpManager.instance.DestroyPowerUp(powerUpBase);
        if (player.transform.localScale.x >= 4)
        {
            player.transform.localScale = new Vector3(player.transform.localScale.x / 2, player.transform.localScale.y, player.transform.localScale.z);
        }
    }

    private void BiggerPlayer(PowerUpBase powerUpBase)
    {
        PowerUpManager.instance.DestroyPowerUp(powerUpBase);
        if (player.transform.localScale.x <= 4)
        {
            player.transform.localScale = new Vector3(player.transform.localScale.x * 2, player.transform.localScale.y, player.transform.localScale.z);
        }
    }

    void FastBall(PowerUpBase powerUpBase)
    {
        PowerUpManager.instance.DestroyPowerUp(powerUpBase);

        foreach (var item in ballContollerList)
        {
            if (item.speed < 20.0f)
                item.speed = item.speed * 2;
        }
    }

    private void SlowBall(PowerUpBase powerUpBase)
    {
        PowerUpManager.instance.DestroyPowerUp(powerUpBase);
        foreach (var item in ballContollerList)
        {
            if (item.speed > 5.0f)
                item.speed = item.speed / 2;
        }
    }

    void DoubleBall(PowerUpBase powerUpBase)
    {
        PowerUpManager.instance.DestroyPowerUp(powerUpBase);
        CreateBall();
    }

    void CreateBall()
    {
        BallContoller ballContoller = Instantiate(prefabBall).GetComponent<BallContoller>();
        ballContollerList.Add(ballContoller);
        ballContoller.gameObject.transform.position = new Vector3(0, 0.5f, -6);
        ballContoller.BallFirstMove();
    }

    public void Destroyball(BallContoller contoller)
    {
        Destroy(contoller.gameObject);
        ballContollerList.Remove(contoller);
    }
}
